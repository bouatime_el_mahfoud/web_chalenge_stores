import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators} from '@angular/forms';
import {AuthService } from '../../../../../services/authService.service'
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
signInForm:FormGroup;
  constructor(private formBuilder:FormBuilder,private authService:AuthService) { }

  ngOnInit() {
  this.initForm();
  }
  initForm(){
  	this.signInForm=this.formBuilder.group(
  	{
  		login:['',[Validators.required,Validators.email]],
  		password:['',[Validators.required,Validators.minLength(8)]]
  	}
  	)
  }
  onSubmitSignInForm(){
  		this.authService.signIn(this.signInForm.value["login"],this.signInForm.value["password"]);
  }

}
