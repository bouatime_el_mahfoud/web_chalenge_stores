import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sign-menu',
  templateUrl: './sign-menu.component.html',
  styleUrls: ['./sign-menu.component.css']
})
export class SignMenuComponent implements OnInit {
  login:boolean;
  constructor() { }

  ngOnInit() {
  this.login=false;
  }
  onSwitch(){
  		this.login=!this.login;
  }

}
