import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators} from '@angular/forms';
import {AuthService } from '../../../../../services/authService.service'
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  signUpForm:FormGroup;
  constructor(private formBuilder:FormBuilder,private authService:AuthService) { }

  ngOnInit() {
  this.initForm();
  	}

  initForm(){
  	this.signUpForm=this.formBuilder.group(
  	{
  		login:['',[Validators.required,Validators.email]],
  		password:['',[Validators.required,Validators.minLength(8)]]
  	}
  	)
  }
  onSubmitSignUpForm(){
    this.authService.signUp(this.signUpForm.value)
  }


}
