import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/authService.service'

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {
  
  constructor(private authService:AuthService) { }

  ngOnInit() {
  	
  }
  onLogout(){
  	this.authService.logout();
  }


}
