import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListItemsPreferedComponent } from './list-items-prefered.component';

describe('ListItemsPreferedComponent', () => {
  let component: ListItemsPreferedComponent;
  let fixture: ComponentFixture<ListItemsPreferedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListItemsPreferedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListItemsPreferedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
