import { Component, OnInit } from '@angular/core';
import { Shop } from '../../../models/shop.model';
import { ShopService } from '../../../services/shopService.service';
import { Subscription } from 'rxjs/subscription';
@Component({
  selector: 'app-list-items-prefered',
  templateUrl: './list-items-prefered.component.html',
  styleUrls: ['./list-items-prefered.component.css']
})
export class ListItemsPreferedComponent implements OnInit {
  preferedShops:Shop[]=[];
  preferedShopsSubscription:Subscription;
  constructor(private shopService:ShopService) { }

  ngOnInit() {
	  this.preferedShopsSubscription=this.shopService.shopSubject.subscribe(
	  		(shops:Shop[])=>{
	  			this.preferedShops=shops;
	  		}
	  	);
	  this.shopService.loadShops(1);
  }

}
