import { Component, OnInit ,Input} from '@angular/core';
import { ShopService } from '../../../../services/shopService.service';
@Component({
  selector: 'app-shop-item-prefered',
  templateUrl: './shop-item-prefered.component.html',
  styleUrls: ['./shop-item-prefered.component.css']
})
export class ShopItemPreferedComponent implements OnInit {
  @Input() shop:any;
  constructor(private shopService:ShopService) { }

  ngOnInit() {
  }
  onRemove(){
  	this.shopService.removeStore(this.shop.id);
  }

}
