import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopItemPreferedComponent } from './shop-item-prefered.component';

describe('ShopItemPreferedComponent', () => {
  let component: ShopItemPreferedComponent;
  let fixture: ComponentFixture<ShopItemPreferedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopItemPreferedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopItemPreferedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
