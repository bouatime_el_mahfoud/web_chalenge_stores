import { Component, OnInit } from '@angular/core';
import { Shop } from '../../../models/shop.model';
import { ShopService } from '../../../services/shopService.service';
import { Subscription } from 'rxjs/subscription';
@Component({
  selector: 'app-list-items-nearby',
  templateUrl: './list-items-nearby.component.html',
  styleUrls: ['./list-items-nearby.component.css']
})
export class ListItemsNearbyComponent implements OnInit {
  nearbyShops:Shop[]=[];
  nearbyShopsSubscription:Subscription;
  constructor(private shopService:ShopService) { }

  ngOnInit() {
  	this.nearbyShopsSubscription=this.shopService.shopSubject.subscribe(
  		(shops:Shop[])=>{
  			this.nearbyShops=shops;
  		}
  	);
  	this.shopService.loadShops(0);
  }

}
