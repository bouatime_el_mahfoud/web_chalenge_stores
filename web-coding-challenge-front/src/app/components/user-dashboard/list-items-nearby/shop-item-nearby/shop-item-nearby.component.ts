import { Component, OnInit ,Input } from '@angular/core';
import { ShopService } from '../../../../services/shopService.service';
@Component({
  selector: 'app-shop-item-nearby',
  templateUrl: './shop-item-nearby.component.html',
  styleUrls: ['./shop-item-nearby.component.css']
})
export class ShopItemNearbyComponent implements OnInit {
  @Input() shop:any;
  constructor(private shopService:ShopService) { }

  ngOnInit() {
  }
  onDislike(){
  	this.shopService.likeOrDislikeShop(this.shop.id,false)
  }
  onLike(){
  	this.shopService.likeOrDislikeShop(this.shop.id,true)
  }
}
