import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopItemNearbyComponent } from './shop-item-nearby.component';

describe('ShopItemNearbyComponent', () => {
  let component: ShopItemNearbyComponent;
  let fixture: ComponentFixture<ShopItemNearbyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopItemNearbyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopItemNearbyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
