import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListItemsNearbyComponent } from './list-items-nearby.component';

describe('ListItemsNearbyComponent', () => {
  let component: ListItemsNearbyComponent;
  let fixture: ComponentFixture<ListItemsNearbyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListItemsNearbyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListItemsNearbyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
