import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { AcceuilComponent } from './components/landing-page/acceuil/acceuil.component';
import { SignInComponent } from './components/landing-page/acceuil/sign-menu/sign-in/sign-in.component';
import { SignUpComponent } from './components/landing-page/acceuil/sign-menu/sign-up/sign-up.component';
import { SignMenuComponent } from './components/landing-page/acceuil/sign-menu/sign-menu.component';
import { WelcomeComponent } from './components/landing-page/acceuil/welcome/welcome.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { AuthService } from './services/authService.service';
import { AuthGuardService } from './services/authGuardService.service';
import { ShopService } from './services/shopService.service';
import { UserDashboardComponent } from './components/user-dashboard/user-dashboard.component';
import { ListItemsNearbyComponent } from './components/user-dashboard/list-items-nearby/list-items-nearby.component';
import { ListItemsPreferedComponent } from './components/user-dashboard/list-items-prefered/list-items-prefered.component';
import { ShopItemNearbyComponent } from './components/user-dashboard/list-items-nearby/shop-item-nearby/shop-item-nearby.component';
import { ShopItemPreferedComponent } from './components/user-dashboard/list-items-prefered/shop-item-prefered/shop-item-prefered.component';



@NgModule({
  declarations: [
    AppComponent,
    AcceuilComponent,
    SignInComponent,
    SignUpComponent,
    SignMenuComponent,
    WelcomeComponent,
    LandingPageComponent,
    UserDashboardComponent,
    ListItemsNearbyComponent,
    ListItemsPreferedComponent,
    ShopItemNearbyComponent,
    ShopItemPreferedComponent,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AuthService,AuthGuardService,ShopService],
  bootstrap: [AppComponent]
})
export class AppModule { }
