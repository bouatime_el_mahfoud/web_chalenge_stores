import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
@Injectable()
export class AuthService{
	storageKey:string="token";
	constructor(private httpClient:HttpClient,private router:Router){

	}

	setToken(token:string){
		localStorage.setItem(this.storageKey,token);
	}
	getToken(){
		return localStorage.getItem(this.storageKey);
	}

	signIn(email:string,password:string){
	this.httpClient.post('http://localhost:8000/api/login',{email:email,password:password}).subscribe(
	(res:any)=>{
		this.setToken('Bearer '+res.success.token);
		this.router.navigate(["/dashboard"]);
	},
	err=>{
		if(err.status==400){
			alert('incorrect email or password');
		}
		else {
		alert("error,please try later");
		}
	}

	);
	}
	signUp(user:any){
		
		this.httpClient.post('http://localhost:8000/api/signup',{email:user.login,password:user.password}).subscribe(
			res=>{alert('registered');
			},
			err=>{
			if(err.status==400){
			alert("this email already exist");
			}
			else {
				alert("error,please try later");
			}
			
			}
		);
	}
	logout(){
		localStorage.removeItem(this.storageKey);
		this.router.navigate(['/']);

	}

}