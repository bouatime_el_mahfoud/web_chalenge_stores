import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { HttpClient ,HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import {AuthService } from './authService.service';
import { Shop } from '../models/shop.model';

@Injectable()
export class ShopService {
	

	constructor(private httpClient:HttpClient,private authService:AuthService,private router:Router){
	
	}

	private shops:Shop[]=[];
	
	shopSubject=new Subject<Shop[]>()
	
	emitShopSubject(){
		this.shopSubject.next(this.shops.slice());
	}

	loadShops(type:number){
		this.shops=[];
		let headers = new HttpHeaders({'Accept':'application/json','Authorization':this.authService.getToken()});
		this.httpClient.get('http://localhost:8000/api/shops/likes?type='+type,{headers:headers}).subscribe(
		(response:any)=>{

			for(var i=0 ;i<response.shops.length;i++){
				this.shops.push(new Shop(response.shops[i].name,response.shops[i].id));
			}
			this.emitShopSubject();
		},
    	(error)=>{
    	console.log("une erreur s'est produit :"+error);
   	    }
		);
	}
	likeOrDislikeShop(id_shop:number,like:boolean){
		
		let headers = new HttpHeaders({'Accept':'application/json','Authorization':this.authService.getToken()});
		this.httpClient.post('http://localhost:8000/api/shops/likes',{shop_id:id_shop,like:like},{headers:headers}).subscribe(
		(res)=>{
			if(like){
				this.router.navigate(["/dashboard/list-prefered-shops"]);
			}
			else{
				this.loadShops(0);
			}
		}
		,(error)=>{
			
		}
		);
		
	}
	removeStore(id_shop:number){
		let headers = new HttpHeaders({'Accept':'application/json','Authorization':this.authService.getToken()});
		this.httpClient.delete('http://localhost:8000/api/shops/likes?like_id='+id_shop,{headers:headers}).subscribe(	
		(res:any)=>{
		this.router.navigate(["/dashboard/list-nearby-shops"])
		},
		(error)=>{
		
		}
	);
	}
	

	

}