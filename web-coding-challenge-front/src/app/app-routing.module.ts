import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { UserDashboardComponent } from './components/user-dashboard/user-dashboard.component'
import { ListItemsNearbyComponent } from './components/user-dashboard/list-items-nearby/list-items-nearby.component';
import { ListItemsPreferedComponent } from './components/user-dashboard/list-items-prefered/list-items-prefered.component';
import { AuthGuardService } from './services/authGuardService.service';
const routes: Routes = [
{path:"",component:LandingPageComponent},
{path:"dashboard",component:UserDashboardComponent,canActivate:[AuthGuardService],children:[
	{path:"",component:ListItemsNearbyComponent},
	{path:"list-prefered-shops",component:ListItemsPreferedComponent},
	{path:"list-nearby-shops",component:ListItemsNearbyComponent},

]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
