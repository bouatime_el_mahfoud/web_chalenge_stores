<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function shops(){
        return $this->belongsToMany('App\Shop','user_shops')->withTimestamps()->withPivot('status')->orderBy('distance', 'desc');;
    }
    public function nearByShops()
    {
    $ids = \DB::table('user_shops')->where('user_id', '=', $this->id)->pluck('shop_id');
    return Shop::whereNotIn('id', $ids)->get();
    }
}
