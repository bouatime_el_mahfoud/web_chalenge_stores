<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth; 
use App\User;

class UserController extends Controller
{
    //
    public $successStatus = 200;
    public function login(Request $request){

    	$validator=Validator::make($request->all(), [ 
            'email' => 'required|email', 
            'password' => 'required|min:8', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
        	$user=Auth::user(); 
        	$success['token'] =  $user->createToken('welcome to United Remote Shops  app')-> accessToken; 
            
        	return response()->json(['success' => $success], $this-> successStatus); 
        }
        else{
        	return response()->json(['error'=>'Unauthorised'], 400);
        } 
     }

     public function signUp(Request $request){
     	$validator=Validator::make($request->all(), [ 
            'email' => 'required|email|unique:users', 
            'password' => 'required|min:8',
      


        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 400);            
        }
        $user=new User(
        [
           
            'email' => $request->email,
            'password' => bcrypt($request->password),

        ]);

        $user->save();
      
        $success['token'] =  $user->createToken('welcome to United Remote Shops  app')-> accessToken; 
        return response()->json(['success' => $success], $this-> successStatus);
        
     }
}
