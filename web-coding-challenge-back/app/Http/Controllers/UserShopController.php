<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Shop;
use App\UserShop;
class UserShopController extends Controller
{
    //the store method is a methode to execute the logic of liking or disliking a store ,it responds to a post request sent to "http://localhost:8000/api/shops/likes". this method requires authentication ,please check the api routes .


    public function store(Request $request){
    	//a validation to check the http request parameters
    	$validator=Validator::make($request->all(), [ 
            'shop_id' => 'required|exists:shops,id', //the parameter shop_id is required and should existin the table shoops
            'like'=>'required|boolean'               //the parameter like is required and it's a boolean
            
        ]);
        //if the validation failed we return a description of the error
        if($validator->fails()){
        	 return response()->json(['error'=>$validator->errors()], 401);
        }
        //we check if the operation required is to like or dislike a shop
        if($request->like)$like="like";
        else $like="dislike";
        //we get the user and we attatch it with the shoop we liked or disliked
        $user=$request->user();
        $shop=Shop::find($request->shop_id);
        $user->shops()->attach($shop,["status"=>$like]);
        //if everything is ok we return a success response
        return response()->json(['success'=>true], 200);
    }



    // the get method take care of returning either the nearby shops  or the prefered shops ,this method respond to a get request sent to "http://localhost:8000/api/shops/likes" ,this method requires authentication ,please check the api routes .



    public function get(Request $request){
    	//  a validation is preformed on the http request parameters 
    	$validator=Validator::make($request->all(), [ 
            'type' => 'required|boolean'     // the type parameter is required and is a boolean it represents the type of the shops we are retriving nearby,prefered
            
        ]);

        //if the validation failed we return a description of the error
        if($validator->fails()){
        	 return response()->json(['error'=>$validator->errors()], 401);
        }
        // i verify if there is any shops that are  disliked this is 2 hours
        $date = new \DateTime;
	    $date->modify('-120 minutes');
		$formatted = $date->format('Y-m-d H:i:s');
        UserShop::where([['updated_at', '<=',$formatted],['status','dislike']])->delete();

        // i am retriving the user and preparing an array that will hold my result
        $user=$request->user();
        $result=[];
        // i am gona check what type of shops do i need is it a 0:nearby shops or 1 :my prefered shops
        if(!$request->type){
        	//i wrote a function to allow me to retrieve the nearby shops ,you can check it in App/user ,it takes on consideration to include just the shops who arent liked or disliked
        	return response()->json(['shops'=>$user->nearByShops()], 200);
        }
        else {
        	$shops=$user->shops;
        	foreach ($user->shops as $shop) {
        		if($shop->pivot->status=="like"){
        			array_push($result,$shop);
        		}
        	}
        	return response()->json(['shops'=>$result], 200);
        }
    	
    }



    // this methode take care of deleting a prefered store ,this method respond to a delete request sent  to "http://localhost:8000/api/shops/likes".this method requires authentication ,please check the api routes .



    public function delete(Request $request){
    	//  a validation is preformed on the http request parameters 
    	$validator=Validator::make($request->all(), [ 
            'like_id' => 'required|exists:shops,id'  //the parameter like_id represent the prefered_store we are trying to delete ,it's required and should 													exists on the shops table
            
        ]);
        //if the validation failed we return a description of the error 
        if($validator->fails()){
        	 return response()->json(['error'=>$validator->errors()], 401);
        }
        //we get the prefered store that we are trying to delete and the user
        $user=$request->user();
        $shop=Shop::find($request->like_id);
       	$user->shops()->detach($shop);
        //if everything is ok we return a success response
        return response()->json(['success'=>true], 200);
        
 
       
    }
}
