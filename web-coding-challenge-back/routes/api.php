<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'UserController@login');
Route::post('signup', 'UserController@signUp');
Route::group(['middleware' => ['auth:api']], function(){
	Route::get('shops/likes','UserShopController@get');
	Route::post('shops/likes','UserShopController@store');
	Route::delete('shops/likes','UserShopController@delete');
});

